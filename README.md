# PG cluster deploy

Ansible playbook to deploy a postgreSQL cluster.

## Requirements

* You need roles bellow to use this playbook :
  * [Open Nebula VMs deploy](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/one_vms_deploy)
  * [PostgreSQL install](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-install)
  * [PGlift install](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pglift-install)
  * [PGlift instance](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pglift-instance)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Deploy pgsql cluster
  hosts: all
  vars_files:
    - vars/main.yml
  tasks:
    - name: Install PostgreSQL
      ansible.builtin.include_role:
        name: pgsql_install
    - name: Install PGlift
      ansible.builtin.include_role:
        name: pglift_install
    - name: Deploy PostgreSQL instance
      ansible.builtin.include_role:
        name: pglift_instance
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
